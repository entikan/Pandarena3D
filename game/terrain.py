from panda3d.core import ShaderTerrainMesh
from panda3d.core import Shader
from panda3d.core import DirectionalLight
from panda3d.core import BitMask32
from panda3d.core import SamplerState

from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletHeightfieldShape
from panda3d.bullet import ZUp


def make_terrain(bullet_world, filename='test_heightmap/heightmap'):
    root = render.attach_new_node(filename)
    height = 128
    img_name = '{}.png'.format(filename)
    img = loader.load_texture(img_name)
    width = img.get_x_size()
    offset = width / 2.0 - 0.5

    shape = BulletHeightfieldShape(img, height, ZUp)
    node = BulletRigidBodyNode(filename)
    node.add_shape(shape)
    bullet_world.attach_rigid_body(node)

    terrain = ShaderTerrainMesh()
    terrain.set_heightfield(img)
    terrain.set_target_triangle_width(20)
    terrain.set_update_enabled(True)
    terrain.set_chunk_size(32)
    terrain_root = root.attach_new_node(terrain)
    terrain_shader = Shader.load(
        Shader.SL_GLSL, '{}.vert'.format(filename), '{}.frag'.format(filename)
    )
    terrain_root.set_shader(terrain_shader)
    terrain_root.set_shader_input("camera", base.cam)
    terrain_root.set_scale(1024, 1024, height)
    terrain_root.set_pos(-offset, -offset, -height/2)
    terrain.generate()

    grass_tex = loader.loadTexture("test_heightmap/ground.png")
    grass_tex.set_minfilter(SamplerState.FT_linear_mipmap_linear)
    grass_tex.set_anisotropic_degree(16)
    terrain_root.set_texture(grass_tex)

    objects = loader.load_model('{}.bam'.format(filename))
    for rigidbody in objects.find_all_matches("**/+BulletRigidBodyNode"):
        rigidbody.set_collide_mask(BitMask32.bit(0))
        bullet_world.attach_rigid_body(rigidbody.node())
    objects.reparent_to(render)

    def add_light():
        light = render.attach_new_node(DirectionalLight('sun'))
        light.set_pos(15000,10000/2,10000)
        light.look_at(render)
        light.node().set_color((0.4,1,0.2,1))
        render.set_light(light)
    add_light()
