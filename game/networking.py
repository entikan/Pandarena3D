from panda3d.core import ConfigVariableInt
from panda3d.core import URLSpec, ConfigVariableInt, ConfigVariableString

from direct.distributed.ServerRepository import ServerRepository
from direct.distributed.ClientRepository import ClientRepository
from direct.distributed.DistributedSmoothNode import DistributedSmoothNode
from direct.actor.Actor import Actor


class BaseClientRepository(ClientRepository):
    def __init__(self, dcSuffix='', ip='127.0.0.1', port=4400):
        dcFileNames = ['direct.dc']
        ClientRepository.__init__(
            self,
            dcFileNames = dcFileNames,
            dcSuffix = dcSuffix,
            threadedNet = True)

        tcpPort = ConfigVariableInt('server-port', port).getValue()
        hostname = ConfigVariableString('server-host', ip).getValue()
        url = URLSpec('http://{}:{}'.format(hostname, tcpPort))
        self.connect([url],
            successCallback = self.connectSuccess,
            failureCallback = self.connectFailure)

    def connectSuccess(self):
        self.accept('createReady', self.gotCreateReady)

    def connectFailure(self, statusCode, statusString):
        base.messenger.send('client-not-joined')

    def gotCreateReady(self):
        if not self.haveCreateAuthority():
            return
        self.ignore('createReady')

    def lostConnection(self):
        exit()


class AIRepository(BaseClientRepository):
    def __init__(self):
        BaseClientRepository.__init__(self,'AI')

    def gotCreateReady(self):
        if not self.haveCreateAuthority():
            return
        self.ignore('createReady')
        self.timeManager = self.createDistributedObject(
            className = 'TimeManagerAI', # The Name of the Class we want to initialize
            zoneId = 1) # The Zone this Object will live in

    def deallocateChannel(self, doID):
        print("Client left us: ", doID)


class GameClientRepository(BaseClientRepository):
    def __init__(self):
        BaseClientRepository.__init__(self)
        self.distributedObject = None
        self.aiDGameObect = None

    def connectSuccess(self):
        self.setInterestZones([1])
        self.acceptOnce(self.uniqueName('gotTimeSync'), self.syncReady)

    def gotCreateReady(self):
        if not self.haveCreateAuthority():
            return
        self.ignore(self.uniqueName('createReady'))
        self.join()
        print("Client Ready")

    def syncReady(self):
        if self.haveCreateAuthority():
            self.gotCreateReady()
        else:
            self.accept(self.uniqueName('createReady'), self.gotCreateReady)

    def join(self):
        self.setInterestZones([1, 2])
        base.messenger.send('client-joined')
        print("Joined")


class GameServerRepository(ServerRepository):
    def __init__(self):
        tcpPort = ConfigVariableInt('server-port', 4400).getValue()
        dcFileNames = ['direct.dc']
        ServerRepository.__init__(self, tcpPort, dcFileNames=dcFileNames, threadedNet=True)


class DistributedSmoothActor(DistributedSmoothNode, Actor):
    def __init__(self, cr):
        Actor.__init__(self, "models/ralph",
            {"run": "models/ralph-run",
            "walk": "models/ralph-walk"})
        DistributedSmoothNode.__init__(self, cr)
        self.setCacheable(1)
        self.setScale(.2)

    def generate(self):
        DistributedSmoothNode.generate(self)
        self.activateSmoothing(True, False)
        self.startSmooth()

    def announceGenerate(self):
        DistributedSmoothNode.announceGenerate(self)
        self.reparentTo(render)

    def disable(self):
        self.stopSmooth()
        if (not self.isEmpty()):
            Actor.unloadAnims(self, None, None, None)
        DistributedSmoothNode.disable(self)

    def delete(self):
        try:
            self.DistributedActor_deleted
        except:
            self.DistributedActor_deleted = 1
            DistributedSmoothNode.delete(self)
            Actor.delete(self)

    def start(self):
        self.startPosHprBroadcast()

    def loop(self, animName):
        self.sendUpdate("loop", [animName])
        return Actor.loop(self, animName)

    def pose(self, animName, frame):
        self.sendUpdate("pose", [animName, frame])
        return Actor.pose(self, animName, frame)
