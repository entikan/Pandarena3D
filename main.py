import argparse

from direct.showbase.ShowBase import ShowBase

from panda3d.core import load_prc_file, Filename
from panda3d.core import Vec3
from panda3d.bullet import BulletDebugNode
from panda3d.bullet import BulletWorld

import simplepbr

from game.networking import *
from game.terrain import make_terrain
from game.bullet import Player


class Avatar(Player):
    def __init__(self, cr, bullet_world):
        Player.__init__(self, bullet_world)
        self.distribution_node = DistributedSmoothActor(cr)
        cr.createDistributedObject(
            distObj = self.distribution_node, zoneId = 2)
        self.distribution_node.start()

    def update(self):
        self.move()
        # HACK. Write a DistributedBulletRigidBodyActor instead
        self.distribution_node.set_transform(self.root.get_transform(render))


def make_bullet_world():
    bullet_world = BulletWorld()
    bullet_world.set_gravity(Vec3(0, 0, -9.81))
    def bullet_debug():
        bullet_debug = BulletDebugNode('Debug')
        bullet_debug.show_wireframe(False)
        bullet_debug.show_constraints(True)
        bullet_debug.show_bounding_boxes(True)
        bullet_debug.show_normals(False)
        bullet_world.set_debug_node(bullet_debug)
        bullet_debug_np = render.attach_new_node(bullet_debug)
        bullet_debug_np.show()
    bullet_debug()
    make_terrain(bullet_world)
    return bullet_world

def update(task):
    base.player.update()
    base.bullet_world.do_physics(globalClock.get_dt(), 4, 1. /240.)
    return task.cont

def client_joined():
    print("joined, starting multiplayer")
    base.player = Avatar(base.client, base.bullet_world)
    base.task_mgr.add(update)

def client_not_joined():
    print("not joined, starting singleplayer")
    print("run 'python main.py --server' to enable multiplayer")
    base.player = Player(base.bullet_world)
    base.task_mgr.add(update)

if __name__ == "__main__":
    load_prc_file(Filename.expand_from('$MAIN_DIR/settings.prc'))


    parser = argparse.ArgumentParser()
    parser.add_argument('--server',action='store_true')
    args = parser.parse_args()
    if args.server:
        base = ShowBase(windowType='none')
        GameServerRepository()
        AIRepository()
    else:
        base = ShowBase()
        simplepbr.init()
        base.client = GameClientRepository()
        base.bullet_world = make_bullet_world()
        base.accept("client-joined", client_joined)
        base.accept("client-not-joined", client_not_joined)
    base.win.set_clear_color((0,0,0,1))
    base.accept('l', render.ls)
    base.camLens.set_fov(100)
    base.run()
