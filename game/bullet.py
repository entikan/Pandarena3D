from math import sqrt

from panda3d.core import TransformState
from panda3d.core import NodePath
from panda3d.core import Vec3
from panda3d.core import BitMask32

from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletSphereShape
from panda3d.bullet import BulletCapsuleShape
from panda3d.bullet import ZUp

from .networking import DistributedSmoothActor


class GameObject():
    def __init__(self, nodepath):
        self.root = nodepath.parent.attach_new_node(nodepath.name)
        self.model = nodepath
        self.model.reparent_to(self.root)
        self.model.name += '-model'


class BulletPhysicsObject(GameObject):
    def __init__(self, bullet_world, nodepath):
        GameObject.__init__(self, nodepath)
        body = BulletRigidBodyNode(self.root.name)
        body_node = self.root.get_parent().attach_new_node(body)
        body_node.set_transform(self.root.get_transform())
        body_node.set_python_tag("GameObject", self)
        self.root.reparent_to(body_node)
        self.root.set_pos_hpr(0,0,0,0,0,0)
        self.root = body_node
        self.root.set_collide_mask(BitMask32.bit(1))
        self.root.node().set_mass(100)
        self.bullet_world = bullet_world
        self.bullet_world.attach_rigid_body(body_node.node())


class BulletCharacterObject(BulletPhysicsObject):
    def __init__(self, bullet_world, nodepath, size=1.7):
        BulletPhysicsObject.__init__(self, bullet_world, nodepath)
        self.size = size
        self.sweep_shape = BulletSphereShape(size/16)
        shape = BulletCapsuleShape(size/4, (size/2), ZUp)
        self.body = self.root.node()
        self.body.add_shape(shape, TransformState.make_pos((0,0,size/2)))
        self.body.set_inertia((5,5,100))
        self.body.set_friction(0.2)
        self.body.set_restitution(1)
        self.body.set_linear_damping(0.2)
        self.body.set_angular_factor((0,0,0)) # don't tip over
        self.body.set_mass(200)
        self.drag = 0.1
        self.jump = False
        self.jump_height = 10
        self.grounded = False

    def update_physics(self, intention=(0,0,0)):
        # Check if on the ground
        pos = self.root.get_pos(render)
        a = TransformState.make_pos((pos.x, pos.y, pos.z+1))
        b = TransformState.make_pos(pos)
        ground = self.bullet_world.sweep_test_closest(self.sweep_shape, a, b, BitMask32.bit(0))
        self.grounded = ground.has_hit()
        # React to ground
        current_velocity = self.body.get_linear_velocity()
        if self.grounded:
            current_velocity.x /= 1+self.drag
            current_velocity.y /= 1+self.drag
            if self.jump:
                current_velocity.z = self.jump_height
                self.jump = False
            self.body.set_linear_velocity(current_velocity)
            speed_multiplier = 10000
        else:
            self.jump = False
            speed_multiplier = 1000 # Less control in air
        intention = Vec3(intention).normalized()
        intention *= speed_multiplier
        impulse = self.root.get_quat(render).xform(intention)
        self.body.apply_central_force(impulse)
        self.model.look_at(render, self.root.get_pos()+impulse)


class Player(BulletCharacterObject):
    def __init__(self, bullet_world):
        BulletCharacterObject.__init__(self, bullet_world, render.attach_new_node('plr'))
        base.cam.reparent_to(self.root)
        base.cam.set_z(1.7)

    def move(self):
        movement = Vec3(0,0,0)
        if base.mouseWatcherNode.has_mouse():
            mx = -base.mouseWatcherNode.get_mouse_x()*50
            my =  base.mouseWatcherNode.get_mouse_y()*50
            base.win.move_pointer(0, base.win.get_x_size() // 2, base.win.get_y_size() // 2)
            self.root.set_h(self.root, mx)
            base.cam.set_p(base.cam, my)
            if base.mouseWatcherNode.is_button_down('d'):
                movement.x = 1
            if base.mouseWatcherNode.is_button_down('a'):
                movement.x = -1
            if base.mouseWatcherNode.is_button_down('w'):
                movement.y = 1
            if base.mouseWatcherNode.is_button_down('s'):
                movement.y = -1
            if base.mouseWatcherNode.is_button_down('space'):
                self.jump = True
        self.update_physics(movement)

    def update(self):
        self.move()
